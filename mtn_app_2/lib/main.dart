import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';

import 'home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          fontFamily: 'Georgia',
          textTheme: const TextTheme(
            headline1: TextStyle(fontSize: 60.0, fontWeight: FontWeight.bold),
            headline6: TextStyle(fontSize: 20.0, fontStyle: FontStyle.italic),
            bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
          ),
          primarySwatch: Colors.amber,
          primaryColor: Colors.amber),
      home: AnimatedSplashScreen(
        splash: Image.asset('assets/logo.jpg'),
        nextScreen: const HomePage(title: 'Flutter Demo Home Page'),
        splashTransition: SplashTransition.scaleTransition,
        backgroundColor: Colors.amber,
      ),
    );
  }
}
